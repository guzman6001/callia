package controlador;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.Ticket;

/**
 * Navegación
 *
 * @author guzman6001
 *
 */
@ManagedBean(name="navegacionController") // or @ManagedBean(name="user")  
@SessionScoped
public class NavegacionController implements Serializable {
    
    public String acceder() {
        return "/login.xhtml";
    }
    
    public String registrarse() {
        return "/registro.xhtml";
    }
    
    public String crearticket(){
        return "/seguro/crearticket.xhtml";
    }
    
    public String configuracion(){
        return "/seguro/configuracion.xhtml";
    }
}
