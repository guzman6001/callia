package controlador;

import modelo.BaseDeDatos;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import modelo.Usuario;

/**
 * Clase que controla la navegación de las opciones de usuarios
 *
 * @author guzman6001
 */
@ManagedBean(name = "usuarioController") // or @ManagedBean(name="user")  
@SessionScoped
public class UsuarioController implements Serializable {

    private String nombre;
    private String apellido;
    private String password;
    private String username;
    private boolean loggedIn;

    /**
     * Obtener usuarios en base de datos para mostrarlos en la vista
     *
     * @return Usuarios en un arrayList
     */
    public ArrayList getUsuarios() {
        return BaseDeDatos.getUsuarios();
    }

    // NAVEGACION
    /**
     * Autenticación.
     *
     * @return regresa la redirección correspondiente
     */
    public String autenticar() {
        Usuario u = new Usuario(username, password, null, null);
        u = BaseDeDatos.getUsuario(u);
        if (u != null) {
            loggedIn = true;
            nombre = u.getNombre();
            apellido = u.getApellido();
            return "/seguro/index.xhtml";
        }

        // ERROR de LOGIN
        FacesMessage msg = new FacesMessage("No se encontró usuario o contraseña es incorrecta.", "ERROR MSG");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "/login.xhtml";
    }

    public String inicio() {
        System.out.println("AUTENTICANDO...\nRUTA_DE_BASE_DE_DATOS: "+System.getProperty("user.dir"));
        if (loggedIn) {
            return "/seguro/index.xhtml";
        }
        return "/login.xhtml";
    }

    public String salir() {
        loggedIn = false;
        FacesMessage msg = new FacesMessage("Sesión finalizada", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return "/login.xhtml";
    }
    
    /**
     * Agregar un usuario en el sistema, captura los parámetros en la vista y
     * los envía a la base de datos para su registro.
     *
     * @return redirección correspondiente
     */
    public String agregarUsuario() {
        BaseDeDatos.agregarUsuario(new Usuario(username, password, nombre, apellido));
        loggedIn = true;
        return "/seguro/index.xhtml";
    }
    
    public String creditos() {
        if (loggedIn) {
            return "/seguro/creditos.xhtml";
        }
        return "/creditos.xhtml";
    }
    
    public String ayuda() {
        if (loggedIn) {
            return "/seguro/ayuda.xhtml";
        }
        return "/ayuda.xhtml";
    }
    // VALIDACIONES:
    /**
     * valida cadena común
     *
     * @param arg0
     * @param arg1
     * @param arg2 El campo
     * @throws ValidatorException
     */
    public void validarStringNormal(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
        String campo = (String) arg2;
        if (campo.length() < 1 || campo.length() > 16) {
            throw new ValidatorException(new FacesMessage("El campo debe tener entre 1 y 16 caracteres"));
        }
        Pattern pat = Pattern.compile("^[a-zA-ZáéíóúÁÉÍÓÚÑñ ]*$");
        Matcher mat = pat.matcher(campo);
        if (!mat.matches()) {
            throw new ValidatorException(new FacesMessage("El campo no debe poseer caracteres especiales, ni números."));
        }
    }

    public void validarUsername(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
        validarStringAlfanumerico(arg0, arg1, arg2);
        String campo = (String) arg2;
        Usuario u = BaseDeDatos.getUsuarioByUsername(campo);
        if (u != null) {
            throw new ValidatorException(new FacesMessage("El nombre de usuario <" + campo + "> ya existe, debe seleccionar otro."));
        }
    }

    public void validarStringAlfanumerico(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
        String campo = (String) arg2;
        if (campo.length() < 1 || campo.length() > 16) {
            throw new ValidatorException(new FacesMessage("El campo debe tener entre 1 y 16 caracteres"));
        }
        Pattern pat = Pattern.compile("^[a-zA-Z0-9_]*$");
        Matcher mat = pat.matcher(campo);
        if (!mat.matches()) {
            throw new ValidatorException(new FacesMessage("El campo solo acepta letras (A-Z, a-z) y números (0-9)."));
        }
    }
    
    // PROPIEDADES (GETTER Y SETTER):
    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }
    
    public String getFullname() {
        return nombre+" "+apellido;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
