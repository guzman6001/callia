package controlador;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpSession;
import modelo.BaseDeDatos;
import modelo.Comentario;
import modelo.Ticket;
import modelo.Usuario;

/**
 * Clase que controla la navegación de las opciones de tickets
 *
 * @author guzman6001
 */
@ManagedBean(name = "ticketController") // or @ManagedBean(name="user")  
@SessionScoped
public class TicketController implements Serializable {

   // ATRIBUTOS:
    private String codigo;
    private String titulo;
    private String descripcion;
    private String tipo;
    private String prioridad;
    private Date fechaApertura;
    private String nuevoAsignado;
    private Date fechaCierre;
    private ArrayList<Comentario> comentarios;
    private String nuevoComentario;
    private Usuario asignado;
    private Usuario creador;
    private String evaluacion; // {A,B,C,D}
    private Ticket selectedTicket;

    // NAVEGACION:
    public String crearticket(){
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        UsuarioController usuarioActual =(UsuarioController) session.getAttribute("usuarioController");
        Usuario usuario = BaseDeDatos.getUsuarioByUsername(usuarioActual.getUsername());
        BaseDeDatos.agregarTicket(new Ticket(titulo, descripcion,  usuario, tipo, prioridad));
        titulo="";
        descripcion="";
        return "/seguro/index.xhtml";
    }
    
    public String modificarticket(){
        BaseDeDatos.modificarTicket(selectedTicket);
        return "/seguro/index.xhtml";
    }
    
    public String agregarComentario(){
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        UsuarioController usuarioActual =(UsuarioController) session.getAttribute("usuarioController");
        Usuario usuario = BaseDeDatos.getUsuarioByUsername(usuarioActual.getUsername());
        Comentario nuevoC = new Comentario(usuario, nuevoComentario);
        BaseDeDatos.agregarComentarioTicket(selectedTicket, nuevoC);
        nuevoComentario="";
        return "/seguro/index.xhtml";
    }
    
    public String cerrarTicket(){
        if ("".equalsIgnoreCase(evaluacion)) evaluacion="No calificada";
        FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
        UsuarioController usuarioActual =(UsuarioController) session.getAttribute("usuarioController");
        Usuario usuario = BaseDeDatos.getUsuarioByUsername(usuarioActual.getUsername());
        Comentario nuevoC = new Comentario(usuario, nuevoComentario);
        BaseDeDatos.cerrarTicket(selectedTicket, evaluacion, nuevoC);
        nuevoComentario="";
        
        return "/seguro/index.xhtml";
    }
    
    public String asignarTicket(){
        Usuario usuario = BaseDeDatos.getUsuarioByUsername(nuevoAsignado);
        BaseDeDatos.asignarTicket(selectedTicket, usuario);
        return "/seguro/index.xhtml";
    }
    
    // FUNCIONES IMPORTANTES
    public ArrayList getTickets() {
        return BaseDeDatos.getTickets();
    }
    
    public Ticket getSelectedTicket() {
        return selectedTicket;
    }
    
    // GETTERS Y SETTERS:
    public TicketController() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public ArrayList<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public void addComentario(Comentario c)
    {
        this.comentarios.add(c);
    }
    
    public Usuario getAsignado() {
        return asignado;
    }

    public void setAsignado(Usuario asignado) {
        this.asignado = asignado;
    }

    public Usuario getCreador() {
        return creador;
    }

    public void setCreador(Usuario asignador) {
        this.creador = asignador;
    }

    public String getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(String evaluacion) {
        this.evaluacion = evaluacion;
    }

    public void setSelectedTicket(Ticket selectedTicket) {
        this.selectedTicket = selectedTicket;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }
    
    // VALIDADORES:
    public void validarNoVacio(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
        String campo = (String) arg2;
        if ("".equalsIgnoreCase(campo)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", "Este campo es obligatorio."));
        }
    }

    public String getNuevoComentario() {
        return nuevoComentario;
    }

    public void setNuevoComentario(String nuevoComentario) {
        this.nuevoComentario = nuevoComentario;
    }

    public String getNuevoAsignado() {
        return nuevoAsignado;
    }

    public void setNuevoAsignado(String nuevoAsignado) {
        this.nuevoAsignado = nuevoAsignado;
    }
}
