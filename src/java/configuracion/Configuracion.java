/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package configuracion;

import com.db4o.Db4oEmbedded;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.config.encoding.StringEncodings;
import modelo.Ticket;

/**
 *
 * @author guzman6001
 */
public class Configuracion {
    public static String RUTA_BASE_DE_DATOS="callia.yap";
    
    public static EmbeddedConfiguration getBasicDBConfig(){
        EmbeddedConfiguration config = Db4oEmbedded.newConfiguration();
        config.common().stringEncoding(StringEncodings.utf8());
        config.common().updateDepth(20);
        return config;
    }
}
