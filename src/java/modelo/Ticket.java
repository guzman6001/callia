package modelo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author guzman6001
 */
public class Ticket implements Serializable{
    
    // ATRIBUTOS:
    private String codigo;
    private String titulo;
    private String descripcion;
    private Date fechaApertura;
    private Date fechaCierre;
    private String tipo;
    private String prioridad;
    private ArrayList<Comentario> comentarios;
    private Usuario asignado;
    private Usuario creador;
    private String evaluacion; // {A,B,C,D}

    public Ticket() {
    }

    public Ticket(String titulo, String descripcion, Usuario creador, String tipo, String prioridad) {
        this.codigo=String.valueOf((new Date()).getTime());
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.creador = creador;
        if ("".equalsIgnoreCase(tipo)) tipo="Indeterminado";
        this.tipo=tipo;
        if ("".equalsIgnoreCase(prioridad)) prioridad="Indeterminada";
        this.prioridad=prioridad;
        this.asignado=creador;
        this.fechaApertura=new Date();
        comentarios=new ArrayList<>();
        this.fechaCierre=null;
        this.evaluacion="";
        
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaApertura() {
        return fechaApertura;
    }
    
    public String getFechaAperturaDDMMYYYY() {
        return new SimpleDateFormat("dd-MM-yyyy (hh:mm aa)").format(fechaApertura);
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public ArrayList<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(ArrayList<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public void addComentario(Comentario c)
    {
        this.comentarios.add(c);
    }
    
    public Usuario getAsignado() {
        return asignado;
    }

    public void setAsignado(Usuario asignado) {
        this.asignado = asignado;
    }

    public Usuario getCreador() {
        return creador;
    }

    public void setCreador(Usuario asignador) {
        this.creador = asignador;
    }

    public String getEvaluacion() {
        return evaluacion;
    }
    
    public void setEvaluacion(String evaluacion) {
        this.evaluacion = evaluacion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }
    
    
}
