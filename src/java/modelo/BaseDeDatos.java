package modelo;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.config.EmbeddedConfiguration;
import com.db4o.config.encoding.StringEncodings;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.Db4oIOException;
import configuracion.Configuracion;
import java.util.ArrayList;
import java.util.Date;

/**
 * Esta clase es la encargada de acceder a la Base de Datos, ofrece una capa de
 * abstracción para poder reemplazarla por otros métodos de accesos a bases de
 * datos.
 *
 * @author guzman6001
 */
public class BaseDeDatos {

    /**
     * Obtener los usuarios de la base de datos
     *
     * @return lista de usuarios
     */
    public static synchronized ArrayList getUsuarios() {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        ArrayList usuarios = new ArrayList();
        try {
            Usuario prototipo = new Usuario(null, null, null, null);
            usuarios = toArrayList(db.queryByExample(prototipo));
        } catch (Exception e) {
            System.out.println("EXCEPCION...");
            e.printStackTrace();
        } finally {
            db.close();
            return usuarios;
        }
    }

    /**
     * Agrega un usuario a la base de datos
     *
     * @param u Objeto usuario completo
     */
    public static synchronized void agregarUsuario(Usuario u) {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        try {
            db.store(u);
        } catch (Exception e) {
            System.out.println("EXCEPCION...");
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public static Usuario getUsuario(Usuario u) {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        Usuario found = null;
        try {
            ObjectSet resultado = db.queryByExample(u);
            found = (Usuario) resultado.next();
        } finally {
            db.close();
            return found;
        }
    }

    /**
     * Convierte el resultado de la consulta en un arrayList, sino se usa esta
     * función se podría decir que la base de datos no funciona, porque el
     * ObjectSet y el ArrayList no son compatibles
     *
     * @param lista ObjectSet que devuelve la consulta a DB4O
     * @return Devuelve la misma lista pero en un objeto ArrayList
     */
    private static ArrayList toArrayList(ObjectSet<Object> lista) {
        ArrayList results = new ArrayList();
        while (lista.hasNext()) {
            results.add(lista.next());
        }
        return results;
    }

    public static Usuario getUsuarioByUsername(String campo) {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        Usuario u = new Usuario(campo, null, null, null);
        Usuario found = null;
        try {
            ObjectSet resultado = db.queryByExample(u);
            if (resultado.hasNext()) {
                found = (Usuario) resultado.next();
            }
        } finally {
            db.close();
            return found;
        }
    }

    // OPCIONES DE TICKETS:
    public static void agregarTicket(Ticket ticket) {
        
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        try {
            db.store(ticket);
        } catch (Exception e) {
            System.out.println("EXCEPCION...");
            e.printStackTrace();
        } finally {
            db.close();
        }
    }
    
    public static synchronized ArrayList getTickets() {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        ArrayList tickets = new ArrayList();
        try {
            Ticket prototipo = new Ticket();
            tickets = toArrayList(db.queryByExample(prototipo));
        } catch (Exception e) {
            System.out.println("EXCEPCION...");
            e.printStackTrace();
        } finally {
            db.close();
            return tickets;
        }
    }
    
    public static Ticket getTicketByCodigo(String campo) {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        Ticket t = new Ticket();
        t.setCodigo(campo);
        Ticket found = null;
        try {
            ObjectSet resultado = db.queryByExample(t);
            if (resultado.hasNext()) {
                found = (Ticket) resultado.next();
            }
        } finally {
            db.close();
            return found;
        }
    }

    public static void modificarTicket(Ticket ticket) {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        Ticket t = new Ticket();
        t.setCodigo(ticket.getCodigo());
        Ticket found;
        try {
            ObjectSet resultado = db.queryByExample(t);
            if (resultado.hasNext()) {
                found = (Ticket) resultado.next();
                found.setTitulo(ticket.getTitulo());
                found.setTipo(ticket.getTipo());
                found.setPrioridad(ticket.getPrioridad());
                found.setAsignado(ticket.getAsignado());
                found.setDescripcion(ticket.getDescripcion());
                db.store(found);
            }
        } finally {
            db.close();
        }
    }

    public static void agregarComentarioTicket(Ticket ticket, Comentario nuevoC) {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        Ticket t = new Ticket();
        t.setCodigo(ticket.getCodigo());
        Ticket found;
        try {
            ObjectSet resultado = db.queryByExample(t);
            if (resultado.hasNext()) {
                found = (Ticket) resultado.next();
                found.addComentario(nuevoC);
                db.store(found);
                db.store(nuevoC);
            }
        } finally {
            db.close();
        }
    }

    public static void cerrarTicket(Ticket ticket, String evaluacion, Comentario nuevoC) {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        Ticket t = new Ticket();
        t.setCodigo(ticket.getCodigo());
        Ticket found;
        try {
            ObjectSet resultado = db.queryByExample(t);
            if (resultado.hasNext()) {
                found = (Ticket) resultado.next();
                found.addComentario(nuevoC);
                found.setEvaluacion(evaluacion);
                found.setFechaCierre(new Date());
                db.store(found);
                db.store(nuevoC);
            }
        } finally {
            db.close();
        }
    }

    public static void asignarTicket(Ticket ticket, Usuario asignado) {
        ObjectContainer db = Db4oEmbedded.openFile(Configuracion.getBasicDBConfig(), Configuracion.RUTA_BASE_DE_DATOS);
        Ticket t = new Ticket();
        t.setCodigo(ticket.getCodigo());
        Ticket found;
        try {
            ObjectSet resultado = db.queryByExample(t);
            if (resultado.hasNext()) {
                found = (Ticket) resultado.next();
                found.setAsignado(asignado);
                db.store(found);
            }
        } finally {
            db.close();
        }
    }
}
