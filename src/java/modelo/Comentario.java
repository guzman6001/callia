package modelo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author guzman6001
 */
public class Comentario {
    private Date fecha;
    private Usuario usuario;
    private String texto;

    public Comentario() {
    }

    public Comentario(Usuario usuario, String texto) {
        this.fecha = new Date();
        this.usuario = usuario;
        this.texto=texto;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getFechaAperturaDDMMYYYY() {
        return new SimpleDateFormat("dd-MM-yyyy (hh:mm aa)").format(fecha);
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    
}
